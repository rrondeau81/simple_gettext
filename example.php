<?php

/*
 * This is a example of how you can use this library
 */

#define('LOCALE', 'en-gb');
#define('LOCALE', 'fr');
define('LOCALE', 'zh-hant');


/**
 * function that return ta translated value
 *
 * @param $string
 * @param string $context
 * @return string
 */
function __($string, $context='') {
    include_once 'simpleGettext.php';

    $translation_db = 'locales/drupal.' . LOCALE . '.sqlite';
    $translator = new \sg\simpleGettext($translation_db);
    return $translator->getTranslation($string, $context);
}

echo __( "Barbados") . '<br/ ><br/ >';
echo __( "Belarus") . '<br/ ><br/ >';
echo __( "Belgium") . '<br/ ><br/ >';
echo __( "OpenID deleted.") . '<br/ ><br/ >';
echo __( "edit menu") . '<br/ ><br/ >';
echo __( "« first") . '<br/ ><br/ >';
echo __( "last »") . '<br/ ><br/ >';
echo __( "Overlay") . '<br/ ><br/ >';
echo __( "Search is currently disabled.") . '<br/ ><br/ >';
echo __( "Syslog identity") . '<br/ ><br/ >';
echo __( "@count orphaned actions (%orphans) exist in the actions table. !link") . '<br/ ><br/ >';
echo __( "Deleted 1 comment.") . '<br/ >';
