<?php
/**
 * User: richard.rondeau
 * Date: 15-01-23
 * Time: 9:02 AM
 */


namespace sg;


/**
 * Class translator
 * @package sg
 */
class simpleGettext extends \SQLite3 {

    /**
     * @var string sqlite3 database
     */
    private $filename;

    /**
     * Open the database
     * @param string $filename
     */
    function __construct($filename) {
        $this->filename = $filename;
        $this->open($this->filename);
    }

    /**
     * @param $file
     */
    public function loadTranslation($file) {
        return false;
    }

    /**
     * @param $string
     * @param $context
     * @return bool
     */
    public function getTranslation($string, $context='') {
        $msgid   = \SQLite3::escapeString($string);
        $msgctxt = \SQLite3::escapeString($context);
        $translation = $this->querySingle("SELECT msgstr FROM translations WHERE msgid = '{$msgid}' AND msgctxt = '{$msgctxt}'");
        return (isset($translation) ? $translation : $string);
    }


    /**
     * Create the table to store the translations
     */
    private function createTable() {
        try {
            $sql = 'CREATE TABLE IF NOT EXISTS translations ( '.
                'msgid text NOT NULL, '.
                'msgctxt text NOT NULL, '.
                'msgstr text NOT NULL, '.
                'PRIMARY KEY (msgid, msgctxt) '.
                ');';

            $this->query($sql);
        }
        catch(Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

    /**
     * @param $file
     * @param string $format
     */
    public function parsePoFile($file, $format='sqlite') {
        require_once 'PoParser/InterfaceHandler.php';
        require_once 'PoParser/FileHandler.php';
        require_once 'PoParser/PoParser.php';

        // Create table if doesn't exists
        $this->createTable();

        // Load the parser with the po file
        $poParser = new \Sepia\PoParser();
        $entries  = $poParser->parseFile($file);

        $stmt = $this->prepare("INSERT OR REPLACE INTO translations (msgid, msgctxt, msgstr) VALUES (:msgid, :msgctxt, :msgstr)");

        $counter = 0;
        foreach($entries as $key => $entry) {
            $counter++;
            $msgid   = array_key_exists('msgid',   $entry) ? implode("\n", $entry['msgid'])   : '';
            $msgctxt = array_key_exists('msgctxt', $entry) ? implode("\n", $entry['msgctxt']) : '';
            $msgstr  = array_key_exists('msgstr',  $entry) ? implode("\n", $entry['msgstr'])  : '';

            $stmt->bindValue(':msgid',   $msgid,   SQLITE3_TEXT);
            $stmt->bindValue(':msgctxt', $msgctxt, SQLITE3_TEXT);
            $stmt->bindValue(':msgstr',  $msgstr,  SQLITE3_TEXT);

            $stmt->execute();
        }
        echo "$counter translations parsed \n";
    }
}