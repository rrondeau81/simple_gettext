Simple Gettext
=========

Simple alternative to php-gettext

 * Copyright : Richard Rondeau richard.rondeau@gmail.com
 * License : GPL-V3
 * Website : https://bitbucket.org/rrondeau81/simple_gettext
