<?php
// You can only use this function in command line.
if (php_sapi_name() != "cli") {
    die('Access denied, can only be used in command line');
}

if(count($argv) != 3) {
    echo "Usage : php generate.php translation_file database \n";
    echo "Example: php generate.php locales/po4500.fr.po locales/locale.fr.sqlite \n";
    die();
}

include_once 'simpleGettext.php';

$file = $argv[1];
$database = $argv[2];

echo "Generating {$database} from {$file} \n";

$translator = new \sg\simpleGettext($database);
$translator->parsePoFile($file);